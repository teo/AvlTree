{ mkDerivation, base, COrdering, stdenv }:
mkDerivation {
  pname = "AvlTree";
  version = "4.3";
  src = ./.;
  libraryHaskellDepends = [ base COrdering ];
  description = "Balanced binary trees using the AVL algorithm";
  license = stdenv.lib.licenses.bsd3;
}
